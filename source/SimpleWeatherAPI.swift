//
//  SimpleWeatherAPI.swift
//
//  Created by Lim Wern Jie.
//  Copyright © 2019 Lim Wern Jie. All rights reserved.
//

import Foundation
import CoreLocation

public var simpleWeatherAPI: SimpleWeatherAPI {
    if _class == nil {
        _class = SimpleWeatherAPI()
    }
    return _class!
}
private var _class: SimpleWeatherAPI?
public class SimpleWeatherAPI: NSObject, CLLocationManagerDelegate {

    private let API_KEY = "" //PLEASE ADD YOUR API KEY HERE
    
    private var locationManager = CLLocationManager()
    private var allowDataStorage = true
    private let MIN_AUTOUPDATE_TIME: TimeInterval = 3600
     
    private var cachedLocation: CLLocation? {
        didSet {
            if cachedLocation != nil {
                if allowWeatherAutoupdates {
                    if -lastWeatherUpdate.timeIntervalSinceNow >= MIN_AUTOUPDATE_TIME {
                        LOG("Updating weather, last updated a long time ago")
                        updateWeatherInfo { (completion) in
                            LOG("Weather successfully updated: \(completion)")
                        }
                    } else {
                        LOG("Weather is recent, not auto-updating.")
                        printWeatherData()
                    }
                    self.isUpdatingWeather = false
                }
            }
        }
    }
    
    /**
     Allows weather information to be automatically updated when location is updated. Defaults to `true`.
     */
    public var allowWeatherAutoupdates = true
    public enum WeatherInfoUpdateStatus {
        case successful
        case failure(Error?)
        case failureNoAPIKey
        case failureNoLocation
    }
    /**
     Updates weather information via OpenWeatherMap's API immediately.
     */
    public func updateWeatherInfo(completion: @escaping (WeatherInfoUpdateStatus) -> ()) {
        
        if API_KEY.isEmpty {
            completion(.failureNoAPIKey)
            return
        }
        
        if let loc = cachedLocation {
            
            let urlStr = "https://api.openweathermap.org/data/2.5/weather?lat=\(loc.coordinate.latitude)&lon=\(loc.coordinate.longitude)&appid=\(API_KEY)"
            
            //Update weather
            SimpleJSONFetcher.getFrom(URL(string: urlStr)!) { (json, error) in
                
                if (error == nil && self.allowDataStorage) {
                    UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "lastAPIcall_openWeatherAPI")
                    UserDefaults.standard.synchronize()
                }
                
                //Parse JSON
                if let json = json as? [String: Any],
                    let weatherInfo = json["weather"] as? [[String: Any]] {
                    
                    LOG("Weather found and parsed")
                    
                    if weatherInfo.count > 0 {
                        if let weatherIDraw = weatherInfo[0]["id"] as? Int,
                            let weatherIconRaw = weatherInfo[0]["icon"] as? String {
                            
                            let weatherID = String(weatherIDraw)
                            let weatherIcon = "\(weatherIconRaw)"
                            
                            let final = "\(weatherID)\(weatherIcon.last!)"
                            
                            LOG("Weather info found: \(final)")
                            
                            if final.count == 4 {
                                
                                if self.allowDataStorage {
                                    UserDefaults.standard.set(final, forKey: "currentWeather_openWeatherAPIdn")
                                    UserDefaults.standard.synchronize()
                                }
                                
                                LOG("Weather updated")
                                self.printWeatherData()
                                self.isUpdatingWeather = false
                                
                                completion(.successful)
                                return
                                
                            }
                        }
                    }
                }
                
                LOG("Weather failed to update")
                self.printWeatherData()
                
                completion(.failure(error))
            }
        } else {
            completion(.failureNoLocation)
        }
    }
    
    /**
     Gives the weather condition id provided by OpenWeatherMap, including day night info at the back of the result, if weather info is known.

     For example, if the weather condition id is provided is "800" and it's for local daytime (i.e. the sun is still up), the returned value is "800d".
     */
    public var currentWeatherID: String? {
        
        if let id =  UserDefaults.standard.string(forKey: "currentWeather_openWeatherAPIdn") {//ID = ###d or ###n
            return id
        }
        
        return nil
    }
    
    private var isUpdatingWeather = false

    /**
     Returns the time where the API last updated its info. Defaults to 1/1/1970 00:00:00 UTC if the weather was never updated, or that data storage and caching is disabled.
     */
    public var lastWeatherUpdate: Date {
        return Date(timeIntervalSince1970: UserDefaults.standard.double(forKey: "lastAPIcall_openWeatherAPI"))
    }
    
    /**
     Returns current weather (if known) in a human readable format.
     */
    public var currentWeather: String? {
        guard let id = currentWeatherID else {return nil}
        
        switch id {
        case let x where x.dropLast() == "800":
            return "Clear"
        case let x where x.first == "2":
            return "Thunderstorms"
        case let x where x.first == "3":
            return "Drizzle"
        case let x where x.first == "5":
            return "Raining"
        case let x where x.first == "6":
            return "Snowing"
        case let x where x.first == "7":
            return "Atmosphere"
        case let x where x.first == "8":
            if Int(String(x.dropLast().last!)) ?? 0 <= 3 {
                return "Partly Cloudy" //801-803
        } else {
                return "Mostly Cloudy" //804
            }
        default:
            return nil
        }
    }
    /**
     Returns an emoji representing the current weather (if known)
     */
    public var currentWeatherEmoji: String? {
        guard let id = currentWeatherID else {return nil}
        
        switch id {
        case let x where x.dropLast() == "800":
            return x.last != "n" ? "☀️" : "🌙"
        case let x where x.first == "2":
            return "⚡️" //Thunder
        case let x where x.first == "3":
            return "☔️" //Drizzle
        case let x where x.first == "5":
            return "☔️" //Rain
        case let x where x.first == "6":
            return "❄️" //Snow
        case let x where x.first == "7":
            return "☁️" //Mist
        case let x where x.first == "8":
            if Int(String(x.dropLast().last!)) ?? 0 <= 3 {
                return "⛅️" //Partly Cloudy (801,802,803)
            } else {
                return "☁️" //Mostly Cloudy (804)
            }
        default:
            return nil
        }
    }
    
    override init() {
        super.init()
        
        locationManager.stopUpdatingLocation()
        
        locationManager.delegate = self
        
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        #if os(iOS)
        locationManager.pausesLocationUpdatesAutomatically = true
        #endif
    }
    
    /**
     Disables data storage for the class. This prevents weather information and last updated time to be cached in local storage.
     */
    public func disableDataStorage() {
        allowDataStorage = false
    }
    
    public var locationAccessAllowed : Bool {
        let disallowed = CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .restricted
        return !disallowed
    }
    
    public func updateCurrentWeatherIfNecessary() -> Bool {
        if -lastWeatherUpdate.timeIntervalSinceNow >= 3600 {
            startUpdatingWeatherInfo()
            return true
        } else {
            return false
        }
    }
    
    public func startUpdatingWeatherInfo() {
        
        if isUpdatingWeather {
            LOG("Beginning weather info is already being updated.")
            return
        } else {
            isUpdatingWeather = true
            LOG("Beginning weather info update.")
        }
        
        if -lastWeatherUpdate.timeIntervalSinceNow < MIN_AUTOUPDATE_TIME {
            LOG("Weather info is already recent.")
            printWeatherData()
            isUpdatingWeather = false
            return
        }
        
        if startLocationUpdatesWithLocationServices() {
            LOG("Updating using location services.")
        } else {
            updateLocationWithIPaddress {(_) in}
            LOG("Using IP Address location.")
        }
    }
    
    public func startLocationUpdatesWithLocationServices() -> Bool {
        if locationAccessAllowed {
            locationManager.startUpdatingLocation()
            return true
        }
        return false
    }
    public func stopLocationUpdatesWithLocationServices() -> Bool {
        if locationAccessAllowed {
            locationManager.stopUpdatingLocation()
            return true
        }
        return false
    }
    
    public func updateLocationWithIPaddress(completion: @escaping (Bool) -> ()) {
        LOG("Querying IP Address location.")
        
        SimpleJSONFetcher.getFrom(URL(string: "https://ipinfo.io/geo")!) { (json, _) in
            LOG("Processing JSON response for location info.")
            if let json = json as? [String: Any],
                let loc = json["loc"] as? String {
                
                let coor = loc.split(separator: ",")
                
                if coor.count == 2 {
                    if let lat = Double(coor[0]),
                        let long = Double(coor[1]) {
                        LOG("IP Address location found.")
                        self.cachedLocation = CLLocation(latitude: lat, longitude: long)
                        completion(true)
                        return
                    }
                }
            }
            LOG("Failed to find IP address location.")
            self.isUpdatingWeather = false
            completion(false)
        }
    }
    
    public func requestLocationAccess() {
        if !locationAccessAllowed {
            LOG("Requesting location access.")
            #if os(macOS)
            //
            #else
            locationManager.requestWhenInUseAuthorization()
            #endif
        }
    }
    
    private func printWeatherData() {
        LOG("Weather data: \(self.currentWeatherID ?? "<no id>"): \(self.currentWeather ?? "<unknown>") \(self.currentWeatherEmoji ?? "<no icon>")")
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        cachedLocation = nil
        locationManager.stopUpdatingLocation()
        updateLocationWithIPaddress {(_) in}
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        if location.horizontalAccuracy <= 100 {
            if cachedLocation == nil {
                cachedLocation = location
            }
            locationManager.stopUpdatingLocation()
        }
    }
}

fileprivate func LOG(_ str: String) {
    //Comment out the below line to disable logging.
    print("[SimpleWeatherAPI]: " + str)
}
