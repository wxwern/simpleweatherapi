# SimpleWeatherAPI

Easily access current location weather info with a simple OpenWeatherMap API wrapper.

This repository contains source code which allows you to easily access weather information for the current location with OpenWeatherMap's API, either by using detected GPS coordinates (device-based) or IP address lookup (with [ipinfo.io](https://ipinfo.io)). 

You'll need to provide your own API key from [OpenWeatherMap](https://openweathermap.org/api), and optionally from [ipinfo.io](https://ipinfo.io/developers).

Currently available in:
- Swift 5 (compatible with iOS, iPadOS, macOS, tvOS and watchOS)

## Usage

### Swift 5

Include the `SimpleWeatherAPI.swift` and `SimpleJSONFetcher.swift` files in your project.

You can then immediately use the global object `simpleWeatherAPI` in your code, which can be used to fetch weather information easily.


## To-Do
- Add Documentation for Swift code in README.md
